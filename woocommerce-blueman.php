<?php

/* 
Plugin Name: WooCommerce BrewMan Export
Plugin URI: http://www.10degrees.uk
Description: Export Orders for BrewMan
Version: 1.2
Author: Jonny Vaughan
Author URI: http://www.10degrees.uk
*/

class WC_Brewman {
    
    public function __construct() {
        
        add_action('admin_menu', array($this, 'tend_brewman_menu' ));                                        // add admin menu to WordPress
        
        add_action('wp_ajax_tend_get_children', array($this, 'tend_get_children' ));                                  // ajax function for getting parents (admin)
        add_action('wp_ajax_tend_add_child', array($this, 'tend_add_child' ));                                  // ajax function for getting parents (admin)
        add_action('wp_ajax_tend_remove_child', array($this, 'tend_remove_child' ));                                  // ajax function for getting parents (admin)
        register_activation_hook( __FILE__, array($this, 'tend_activate'));
        
        
        if(isset($_GET['download_csv'])) add_action('init', array($this, 'generate_csv'));
    }
    
    // plugin activation hook

    function tend_activate(){

      //Use wp_next_scheduled to check if the event is already scheduled
      $timestamp = wp_next_scheduled( 'tend_create_daily_schedule' );

      //If $timestamp == false schedule daily backups since it hasn't been done previously
      if( $timestamp == false ){
        //Schedule the event for right now, then to repeat daily using the hook 'tend_create_daily_schedule'
        wp_schedule_event( time(), 'daily', 'tend_create_daily_schedule' );
      }

    }


    function tend_brewman_menu() {

        add_submenu_page('woocommerce', 'BrewMan Export', 'BrewMan Export', 'manage_options', 'product_orders', array($this, 'tend_brewman_page'));
    }
    
    // enqueue extra product_orders tracking scripts
    
    function tend_product_orders_scripts() {

        // custom scripts
        wp_register_script( 'wp-product_orders', plugin_dir_url( __FILE__ ) . 'js/product_orders.js', array( 'jquery'), "1.0", true);
        wp_enqueue_script ( 'wp-product_orders');
        wp_localize_script( 'wp-product_orders', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );


    }
    // output markup for reports page
    function tend_brewman_page() {
        
        global $wpdb, $woocommerce;
        
        echo "<div class='wrap'>";
        echo "<h1>".__('Order Export', 'tend_product_orders')."</h1>";

        

            echo "<h3>".get_the_title($_REQUEST["id"])."</h3>";

            $orders = new WP_Query( array(
                                        'numberposts' => -1,
                                        'post_type'   => wc_get_order_types(),
                                        'post_status' => array('wc-processing', 'wc-completed', 'wc-on-hold' ),
                                    ) );

            
            // output some garbage WP friendly html 
            echo '<div id="dashboard-widgets-wrap">
            <div id="video-report-widgets" class="metabox-holder">
                <div id="normal-sortables" class="ui-sortable meta-box-sortable">
                    <!-- BOXES -->
            ';
             echo '<table class="widefat" style="width:100%">';
           
            $count = 0;  
           
            if($orders->have_posts()):
            
                while($orders->have_posts()) { 
                    
                    $orders->the_post();
                    $id = get_the_id();
                    
                    $order = new WC_Order(get_the_id());
                    // "ORDH","10060","0","05/06/14","MBKT110060","ESALES","05/06/14","05/06/14","22 Mill Road","(House not a flat)","Any Street","Any Town","","RG32 4AR","078752242489","Matthew Jones","22 Mill Road","","Any Street","Any Town","","KT17 4AR","Spoke with Gemma",""

                   // print_r($order);
                    
                    echo "<tr><td>ORDH</td>
                    <td>".get_the_id()."</td>
                    <td>0</td>
                    <td>".get_the_date("d/m/y")."</td>
                    <td>".get_post_meta($id, '_customer_user', true)."</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>".get_post_meta($id, '_shipping_address_1', true)."</td>
                    <td>".get_post_meta($id, '_shipping_address_2', true)."</td>
                    <td></td>
                    <td>".get_post_meta($id, '_shipping_city', true)."</td>
                    <td>".get_post_meta($id, '_shipping_state', true)."</td>
                    <td>".get_post_meta($id, '_shipping_postcode', true)."</td>
                    <td>".get_post_meta($id, '_telephone', true)."</td>
                    <td>".get_post_meta($id, '_billing_first_name', true)." ".get_post_meta($id, '_billing_last_name', true)."</td>
                    <td>".get_post_meta($id, '_billing_address_1', true)."</td>
                    <td>".get_post_meta($id, '_billing_address_2', true)."</td>
                    <td></td>
                    <td>".get_post_meta($id, '_billing_city', true)."</td>
                    <td>".get_post_meta($id, '_billing_state', true)."</td>
                    <td>".get_post_meta($id, '_billing_postcode', true)."</td>
                    <td>".$order->customer_note."</td>
                    <td></td>
                    </tr>"; 
                    //$order_meta = get_post_meta(get_the_id());
                    
                    
                    $items = $order->get_items();                       
                    
                    //print_r($order);
                    $i = 1;
                    foreach($items as $item) {
                        //print_r($items);
                        $item_price = $item["item_meta"]["_line_subtotal"][0]/$item["item_meta"]["_qty"][0];
                        
                        // "ORDL","10060","1","GOLDCASE","Gold - 4.4% ABV","20.00","","2","40.00","8.00","1"
                        echo "<tr><td>ORDL</td>
                            <td>".get_the_id()."</td>
                            <td>".$i."</td>
                            <td>".$item["item_meta"]["_product_id"][0]."</td>
                            <td>".$item["name"]."</td>
                            <td>".$item_price."</td>
                            <td></td> 
                            <td>".$item["item_meta"]["_qty"][0]."</td>
                            <td>".$item["item_meta"]["_line_subtotal"][0]."</td>
                            <td>".$item["item_meta"]["_line_tax"][0]."</td>
                            <td></td>
                            </tr>";
                        
                        $i++;
                    }
                    
                }// while
            
            else:
            
                echo "<tr><td colspan='2'>".__('No orders found.')."</td></td>";
            
            endif;
            
            echo "</table>";
            
            echo "<p><a class='button action noprint' href='".site_url()."/wp-admin/admin.php?page=export_orders&download_csv'>Export CSV</a></p>";

            echo '
            </div>
            
        </div>
    </div>';


       
        echo "</div>";

    }
    
    public function tend_create_daily_schedule() {
        $this->generate_csv('file');
    }

    // output csv data
	public function generate_csv($output = 'file') {
        
        global $wpdb, $woocommerce;
        $orders = new WP_Query( array(
                                        'numberposts' => -1,
                                        'post_type'   => wc_get_order_types(),
                                        'post_status' => array('wc-processing', 'wc-completed', 'wc-on-hold' ),
                                    ) );
        
        if($output=='download') {
        
        ini_set('display_errors', 'off');
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: binary");
            header("Content-Disposition: attachment; filename=\"export-orders.csv\";" );

        }
            
        $i = 1;
        
        $file_output = "";

         if($orders->have_posts()):
            
            while($orders->have_posts()) { 
                    
                    $orders->the_post();
                    $id = get_the_id();
                                              
                   $file_output .= "\"ORDH\",\"".get_the_id()."\",\"0\",\"".get_the_date("d/m/y")."\",\"".get_post_meta($id, '_customer_user', true)."\",\"\",\"\",\"\",\"".get_post_meta($id, '_shipping_address_1', true)."\",\"".get_post_meta($id, '_shipping_address_2', true)."\",\"\",\"".get_post_meta($id, '_shipping_city', true)."\",\"".get_post_meta($id, '_shipping_state', true)."\",\"".get_post_meta($id, '_shipping_postcode', true)."\",\"".get_post_meta($id, '_telephone', true)."\",\"".get_post_meta($id, '_billing_first_name', true)." ".get_post_meta($id, '_billing_last_name', true)."\",\"".get_post_meta($id, '_billing_address_1', true)."\",\"".get_post_meta($id, '_billing_address_2', true)."\",\"\",\"".get_post_meta($id, '_billing_city', true)."\",\"".get_post_meta($id, '_billing_state', true)."\",\"".get_post_meta($id, '_billing_postcode', true)."\",\"".$order->customer_note."\",\"\",\"\"\r\n";
                    // "ORDH","10060","0","05/06/14","MBKT110060","ESALES","05/06/14","05/06/14","22 Mill Road","(House not a flat)","Any Street","Any Town","","RG32 4AR","078752242489","Matthew Jones","22 Mill Road","","Any Street","Any Town","","KT17 4AR","Spoke with Gemma",""
                    $order = new WC_Order(get_the_id());
                                              
                    $items = $order->get_items();                       
                    
                    $i = 1;
                    foreach($items as $item) {
                        //print_r($items);
                        $item_price = $item["item_meta"]["_line_subtotal"][0]/$item["item_meta"]["_qty"][0];
                        
                        // "ORDL","10060","1","GOLDCASE","Gold - 4.4% ABV","20.00","","2","40.00","8.00","1"
                        $file_output .= "\"ORDL\",\"".get_the_id()."\",\"".$i."\",\"".$item["item_meta"]["_product_id"][0]."\",\"".$item["name"]."\",\"".$item_price."\",\"\",\"".$item["item_meta"]["_qty"][0]."\",\"".$item["item_meta"]["_line_subtotal"][0]."\",\"".$item["item_meta"]["_line_tax"][0]."\",\"\",\"\"\r\n";
                        
                        $i++;
                    }
                    
            }// while

        else:

            $file_output .= __('No orders found.')."\r\n";

        endif;

        if($output=='download') {
            
            echo $file_output;
            
        }else{
            
            $fp = fopen(realpath(dirname(__FILE__))."/exports/brewman-".date("Ymd-his").".csv", "w");
            fwrite($fp, $file_output);
            fclose($fp);
            echo "Orders Exported. They will be available to Brewman in the next few minutes.";
            
        }

        die();
        
	}
    
}
$WC_Brewman = new WC_Brewman();
